

# from these two functions, we can exercise the most important codebase.
# there are some details that won't be tested here,
# such as the ability to have multiple composers attached to a parser
# or special event handlers added to a parser
# but I am solo-developing right now so 80-20 rule applies.


from exo_json_lib import serialise, deserialise 
from unittest import TestCase
import random
import sys

class Arbitrary(object):

    def items(self):
        return (('a','b'),)

class SerialisationTests(TestCase):

    def test_simple_dict(self):
        result = serialise({'a':'b'})
        self.assertEqual(result, '{"a":"b"}')

    def test_object_interface(self):
        result = serialise(Arbitrary())
        self.assertEqual(result, '{"a":"b"}')
        
    def test_list_with_object(self):
        result = serialise(['hello world', {'a':'b', 'c':1}])
        self.assertEqual(result, '["hello world",{"a":"b","c":1}]')
    
    def test_list_with_float(self):
        result = serialise(['hello world', {'a':'b', 'c':1.09}])
        self.assertEqual(result, '["hello world",{"a":"b","c":1.09}]')

    def test_list_object_list_negative_integer(self):
        result = serialise(['hello world', {'a':'b', 'c':[-1,2,3]}])
        self.assertEqual(result, '["hello world",{"a":"b","c":[-1,2,3]}]')

    def test_list_object_list_true(self):
        result = serialise(['hello world', {'a':'b','c':[1,2,3, True]}])
        self.assertEqual(result, '["hello world",{"a":"b","c":[1,2,3,true]}]')

    def test_list_object_list_false(self):
        result = serialise(['hello world', {'a':'b', 'c':[1,2,3, False]}])
        self.assertEqual(result, '["hello world",{"a":"b","c":[1,2,3,false]}]')

    def test_list_object_list_none(self):
        result = serialise(['hello world', {'a':'b', 'c':[1,2,3, None]}])
        self.assertEqual(result, '["hello world",{"a":"b","c":[1,2,3,null]}]')

    def test_invalid_key(self):
        with self.assertRaises(KeyError):
            result = serialise({1: 'value'})
            self.assertEqual(result, '{1:"value"}')

    def test_larger_than_recursion_limit(self):
        try:
            root_element = {}
            ref_element = root_element
            for i in range(0, sys.getrecursionlimit()*2):
                ref_element['a'] = dict()
                ref_element = ref_element['a']

            result = serialise(root_element)
        except RecursionError:
            self.assertTrue(False, "A recursion error was raised.")
        # We don't care to check the result, just that an exception is not thrown.
        
class DeserialisationTests(TestCase):

    def test_simple_dict(self):
        result = deserialise('{"a":"b"}')
        self.assertEqual(result, {'a':'b'})

    def test_list_with_object(self):
        result = deserialise('["hello world",{"a":"b","c":1}]')
        self.assertEqual(result, ['hello world', {'a':'b', 'c':1}])
    
    def test_list_with_float(self):
        result = deserialise('["hello world",{"a":"b","c":1.09}]')
        self.assertEqual(result, ['hello world', {'a':'b', 'c':1.09}])

    def test_list_object_list_negative_integer(self):
        result = deserialise('["hello world",{"a":"b","c":[-1,2,3]}]')
        self.assertEqual(result, ['hello world', {'a':'b', 'c':[-1,2,3]}])

    def test_list_object_list_true(self):
        result = deserialise('["hello world",{"a":"b","c":[1,2,3,true]}]')
        self.assertEqual(result, ['hello world', {'a':'b','c':[1,2,3, True]}])

    def test_list_object_list_false(self):
        result = deserialise('["hello world",{"a":"b","c":[1,2,3,false]}]')
        self.assertEqual(result, ['hello world', {'a':'b', 'c':[1,2,3, False]}])

    def test_list_object_list_none(self):
        result = deserialise('["hello world",{"a":"b","c":[1,2,3,null]}]')
        self.assertEqual(result, ['hello world', {'a':'b', 'c':[1,2,3, None]}])

    def test_invalid_key(self):
        with self.assertRaises(KeyError):
            result = serialise({1: 'value'})

    def test_simple_dict_ws(self):
        result = deserialise('  {   "a":"b"}')
        self.assertEqual(result, {'a':'b'})

    def test_list_with_object_ws(self):
        result = deserialise('[ "hello world",{"a": "b","c":1}]')
        self.assertEqual(result, ['hello world', {'a':'b', 'c':1}])
    
    def test_list_with_float_ws(self):
        result = deserialise('["hello world",\n{"a":"b","c":1.09}]')
        self.assertEqual(result, ['hello world', {'a':'b', 'c':1.09}])

    def test_list_object_list_negative_integer_ws(self):
        result = deserialise('["hello world",   {"a":"b","c":[-1,2,3]}]')
        self.assertEqual(result, ['hello world', {'a':'b', 'c':[-1,2,3]}])

    def test_list_object_list_true_ws(self):
        result = deserialise('["hello world",{"a":"b","c":[1,2,3,true ]}]')
        self.assertEqual(result, ['hello world', {'a':'b','c':[1,2,3, True]}])

    def test_list_object_list_false_ws(self):
        result = deserialise('["hello world",{"a":"b","c":[1,2,3,\tfalse]}]')
        self.assertEqual(result, ['hello world', {'a':'b', 'c':[1,2,3, False]}])

    def test_list_object_list_none_ws(self):
        result = deserialise('["hello world",{"a":"b","c":[1,2,3,null]}\n]')
        self.assertEqual(result, ['hello world', {'a':'b', 'c':[1,2,3, None]}])

    def test_larger_than_recursion_limit(self):
        try:
            root_element = {}
            ref_element = root_element
            for i in range(0, sys.getrecursionlimit()*2):
                ref_element['a'] = dict()
                ref_element = ref_element['a']

            result = deserialise(serialise(root_element))
        except RecursionError:
            self.assertTrue(False, "A recursion error was raised")
        # We don't care to check the result, just that an exception is not thrown.

    def test_larger_than_nesting_limit_randomised(self):
        with self.assertRaises(RuntimeError):
            for i in range(0,100):
                nesting_limit = random.randint(1, 10000)
                root_element = {}
                ref_element = root_element
                for i in range(0, nesting_limit+1):
                    ref_element['a'] = dict()
                    ref_element = ref_element['a']

                result = deserialise(
                        serialise(root_element),
                        nesting_limit=nesting_limit
                )

    def test_larger_than_nesting_limit_fixed_selection(self):
        with self.assertRaises(RuntimeError):
            for nesting_limit in [1, 100, 1000, 5000, 10000]:
                root_element = {}
                ref_element = root_element
                for i in range(0, nesting_limit+1):
                    ref_element['a'] = dict()
                    ref_element = ref_element['a']

                result = deserialise(
                        serialise(root_element),
                        nesting_limit=nesting_limit
                )

    def test_larger_than_nesting_limit_zero(self):
        root_element = {}
        ref_element = root_element
        nesting_limit = 0
        for i in range(0, nesting_limit+1):
            ref_element['a'] = dict()
            ref_element = ref_element['a']

        with self.assertRaises(RuntimeError):
            result = deserialise(
                    serialise(root_element),
                    nesting_limit=nesting_limit
            )

    def test_nesting_limit_neg(self):
        root_element = {'a': 1}
        nesting_limit = -1
        with self.assertRaises(RuntimeError):
            result = deserialise(
                    serialise(root_element),
                    nesting_limit=nesting_limit
            )

    def test_stack_limit_applied(self):
        with self.assertRaises(RuntimeError):
            result = deserialise('[["a"]]', nesting_limit=1)

    def test_pre_content_invalid_character(self):
        with self.assertRaises(ValueError):
            result = deserialise('x{"a":"b"}')

    def test_object_weird_pre_key_character(self):
        with self.assertRaises(ValueError):
            result = deserialise('{x"a":"b"}')

    def test_invalid_escape_character(self):
        with self.assertRaises(ValueError):
            result = deserialise('{\\x"a":"b"}')

    def test_invalid_hex_digit(self):
        with self.assertRaises(ValueError):
            result = deserialise('{"a":\\uG}')

    def test_non_colon_error(self):
        with self.assertRaises(ValueError):
            result = deserialise('{"a","b"}')

    def test_invalid_character_in_object_when_expecting_comma_or_close(self):
        with self.assertRaises(ValueError):
            result = deserialise('{"a":"b"banana}')

    def test_non_quote_char_when_expecting_object_key(self):
        with self.assertRaises(ValueError):
            result = deserialise('{ hello!"a":"b"}')

    def test_invalid_first_value_character(self):
        with self.assertRaises(ValueError):
            result = deserialise('{ hello!"a":g}')

    def test_invalid_character_when_parsing_array(self):
        with self.assertRaises(ValueError):
            result = deserialise('[u]')

    def test_invalid_characters_in_true(self):
        # e.g tru, thru
        with self.assertRaises(ValueError):
            result = deserialise('trf')

    def test_invalid_characters_in_false(self):
        with self.assertRaises(ValueError):
            result = deserialise('flse')

    def test_invalid_characters_in_null(self):
        with self.assertRaises(ValueError):
            result = deserialise('nll')

    def test_non_digit_char_starting_number(self):
        with self.assertRaises(ValueError):
            result = deserialise('-f')

    def test_non_digit_non_dp_in_number(self):
        with self.assertRaises(ValueError):
            result = deserialise('123f')

    def test_non_digit_non_dp_non_e_in_number(self):
        with self.assertRaises(ValueError):
            result = deserialise('1.0e1f')

    def test_invalid_char_at_start_of_exponent(self):
        with self.assertRaises(ValueError):
            result = deserialise('1.0ef5')

    def test_invalid_exponent_digit(self):
        with self.assertRaises(ValueError):
            result = deserialise('1.0e5f')

    def test_additional_content_after_base_end(self):
        with self.assertRaises(ValueError):
            result = deserialise('1.0x')

    def test_whitespace_after_end(self):
        result = deserialise('true          ')
        self.assertEqual(result, True)

    def test_premature_end(self):
        with self.assertRaises(RuntimeError):
            result = deserialise('{"a":"b"')

    def test_of_valid_exponential_value(self):
        result = deserialise('1.0e1')
        self.assertEqual(result, 10)

    def test_bare_int(self):
        result = deserialise('10')
        self.assertEqual(result, 10)

    def test_bare_string(self):
        result = deserialise('"Hello World"')
        self.assertEqual(result, "Hello World")

    def test_empty_nest_objects(self):
        result = deserialise('{"a":{"a":{}}}')
        self.assertEqual(result, {'a':{'a':{}}})

    def test_nest_objects(self):
        result = deserialise('{"a":{"a":{"a":1}}}')
        self.assertEqual(result, {'a':{'a':{'a':1}}})

    def test_fraction_ends_with_dp(self):
        with self.assertRaises(RuntimeError):
            result = deserialise("1.")

    def test_valid_exponential_integer(self):
        result = deserialise("1E3")
        self.assertEqual(result, 1000)

    def test_exponent_with_plus(self):
        result = deserialise("1e+5")
        self.assertEqual(result, 1e+5)

    def test_exponent_with_neg(self):
        result = deserialise("1e-5")
        self.assertEqual(result, 1e-5)

    def test_exponent_with_incomplete_neg(self):
        with self.assertRaises(ValueError):
            result = deserialise("1e-")

    def test_nested_list_with_spaces(self):
        self.assertEqual(deserialise('[[] ]'), [[]])

    def test_zero_led_float(self):
        self.assertEqual(deserialise('0.01'), 0.01)

    def test_zero_led_float_with_whitespace(self):
        self.assertEqual(deserialise('0.01   '), 0.01)

    def test_zero_led_negative_float(self):
        self.assertEqual(deserialise('-0.01'), -0.01)

    def test_list_zero_led_float(self):
        self.assertEqual(deserialise('[0.01]'), [0.01])

    def test_list_zero_led_negative_float(self):
        self.assertEqual(deserialise('[-0.01]'), [-0.01])

    def test_list_zero_list(self):
        self.assertEqual(deserialise('[0]'), [0])

    def test_object_zero(self):
        self.assertEqual(deserialise('{"a":0}'), {'a':0})

    def test_redundant_comma_in_list(self):
        with self.assertRaises(ValueError):
            deserialise('[1,]')

    def test_nested_array_with_spaces(self):
        deserialise('[[] ]')

    def test_missing_comma_in_list(self):
        with self.assertRaises(ValueError):
            deserialise('[1 true]')

    def test_premature_array_close(self):
        with self.assertRaises(ValueError):
            deserialise('[1,]')

    def test_comma_after_string_in_object(self):
        self.assertEqual(deserialise('{"a":"1","b":2}'), {"a":"1","b":2})

    def test_comma_after_integer_in_object(self):
        self.assertEqual(deserialise('{"a":1,"b":2}'), {"a":1,"b":2})

    def test_comma_after_float_in_object(self):
        self.assertEqual(deserialise('{"a":1.2,"b":2}'), {"a":1.2,"b":2})

    def test_comma_after_exponent_in_object(self):
        self.assertEqual(deserialise('{"a":1e2,"b":2}'), {"a":100,"b":2})

    def test_comma_after_integer_in_array(self):
        self.assertEqual(deserialise('[1, 2]'), [1,2])

    def test_comma_after_float_in_array(self):
        self.assertEqual(deserialise('[1.2, 2]'), [1.2,2])

    def test_comma_after_exponent_in_array(self):
        self.assertEqual(deserialise('[1e2, 2]'), [1e2,2])

    def test_int_at_end_of_object(self):
        self.assertEqual(deserialise('{"a":1}'), {"a":1})

    def test_float_at_end_of_object(self):
        self.assertEqual(deserialise('{"a":1.2}'), {"a":1.2})

    def test_exponent_at_end_of_object(self):
        self.assertEqual(deserialise('{"a":1e2}'), {"a":1e2})

    def test_int_at_end_of_array(self):
        self.assertEqual(deserialise('[1]'), [1])

    def test_float_at_end_of_array(self):
        self.assertEqual(deserialise('[1.2]'), [1.2])

    def test_exponent_at_end_of_array(self):
        self.assertEqual(deserialise('[1e2]'), [1e2])
