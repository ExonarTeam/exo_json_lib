
# Json Library by Exonar

Serialisation of json without recursive functions.

## Problem statement

Using the standard json library, if you create an object that is more nested
the recursion limit, you will get recursion limit exceptions and be unable to
completely parse the JSON you submit, or serialise an object that is so
nested. This is a problem we have observed with a number of json libraries.
As such, this library is designed to use a non-recursive parsing
algorithm so as to avoid this limitation.

Note that the use of nonrecursion has necessarily had a negative impact on
speed. This may be something that can be progressively optimised out, but note
that if your objective is to perform fast serialisation on json of knowably
finite recursion, then this library may not be your best choice. Note that
this library explicitly prefers resilience, clarity and I/O agnosticism to
absolute speed or efficiency.

## How to Use this Library

### Python Version

This library should be compatible with any version of python > 3.2.

### Aliases to help with the most common library subsititution

The library comes with a `loads` and `dumps` function designed to work as
drop-in replacements to the `loads` and `dumps` functions of the standard json
library for the most common use case of deserialising a string and serialising
a python value.

These names are aliases to the preferred nomenclature for this library which
are `serialise` and `deserialise`.

Note that this will likely break if you use any of the optional arguments for
the standard json library `loads` and `dumps` functions, but it means that for
most cases you can simply replace the import line `import json` with
`import exo_json_lib as json`. It is important to understand that this will
probably _not_ work for more complex cases like custom serialisations or
de-serialisations.

### Serialisation of arbitrary objects

There are a number of ways of serialising arbitrary objects.

For classes that you control, there are interfaces you can implement on those
classes which will make them amenable to serialisation.

The simplest of these is the `items(self)` method, which works much the same
ways the `items` method on dictionaries. It should return an iterable of
key/value pairs. The keys _must_ be strings,and the values must be instances of
serialisable types.

For classes which serialise as a known json type, but aren't complex objects,
(for example, if you had implemented a custom Fraction class),
there are two additional interfaces. One is the `json_value` interface, which
is a function with no arguments that should return another a standard python
type to "stand in" for the object as its json value.

For classes where you require direct control, you can use `json_raw` which
must return a string which will be placed directly into the json, unescaped,
without quotes, to stand in for the value. This is really running with the
safeties off, so make sure you know what you are doing.

For classes which you do not own, the `dumps` and `serialise` have the
following additional arguments in their signature:

`def serialise(obj, custom_callbacks=None, callback_chooser=type):`

This allows you to supply a dictionary of custom callbacks which map the
class/type of the objects to be serialised, to a function which outputs
the necessary raw string to be included.

If your mapping of types to serialisations is different (e.g. if you have many
types which use the same serialisation), you may wish to implement a callback
chooser function, which will return the value to be used as the key to the
`custom_callback` dictionary.

By way of a simple example:

```

from decimal import Decimal
from exo_json_lib import serialise

from some_lib import get_json_target

def serialise_decimal(decimal):

	return serialise(float(decimal))

callbacks = {
	Decimal: serialise_decimal
}

serialise(serialise(get_json_target()))

```

Note in the example the keys in the callbacks dictionary are the type of the
object. You can change these keys to something else by changing the
`callback_chooser` argument.

If you supply a value to the `custom_callback` dictionary which is keyed to an
already known python type, your new serialisation will override the standard
serialisation for that type.



### Deserialisation of arbitrary objects

For complex objects, it is possible to supply additional arguments to the
`deserialise`/`loads` methods to customise how they appear in Python.

`deserialise(string, composer_factory=StandardComposer, object_cast=nop_cast, nesting_limit=None):`

In general, you will not have to override the `composer_factory` argument,
unless you want to alter how types such as floats, booleans or strings are
natively translated into python types. Creating a new composer is a complex
subject, but you can use the standard composer as a template for how this can
be done, and the body of the deserialise function will show you how to wire
them up together.

The `object_cast` argument is called on every json object to be cast into
python. A json object is translated as a python dictionary, which is then
passed to the `object_cast` callable. The `nop_cast` argument default simply
returns the same dictionary, but you can override this to do anything you like
with complex objects (including re-casting it's attributes).

For example:

```
class MyClass(object):

	def __init__(self, some_int, some_str):
		self.my_int = some_int
		self.my_str = some_str

def my_object_cast(dict_from_json):

	if dict_from_json.get('class_name') == 'MyClass':
		return MyClass(
			dict_from_json['some_int'],
			dict_from_json['some_str']
			)
	else:
		return dict_from_json

```

Note that the decision making code about which json objects should be
converted to which python class can be as complex as you would choose.

### Nesting Limit

For deserialisation, to protect against untrusted input, you can impose a
nesting limit as an optional named argument to `deserialise`, `nesting_limit`.
`None` means no nesting limit, elsewise, a `RuntimeError` will be raised when
the nesting of the python objects exceeds the limit set.

### I/O agnostic interface

If you are processing bytes from a stream or an asynchronous interface rather
than a simple entirely loaded string, you may wish to make use of the i/o
agnostic interface for processing which is on the Parser class.

The interface is made up of five parts:

1. The `__init__` method on the parser. This accepts one argument which is an
iterator which returns a character for each iteration. The simplest iterator I
can think of that matches this description is `iter(<some string>)`, which is
the default call, but in principle you can place here any type which iterates
and returns characters in it's iteration steps. We also provide
`exo_json_lib.char_data_queue.CharDataQueue`, which allows you to
asynchronously add string data to an object, which will then iterate through
it character by character. This is ideal for use with `asyncio`.
2. `step_char`: makes the parser process a single character, and returns a
value indicating whether that processing triggered any events, or reached the
end of the content currently available for the iterator. This is useful if you
want to process your iterator in chunks of a fixed size.
3. `step_event`: makes the parser process as far as the next event (e.g.
"I found a string character"). Will also return if the end of currently
available content is reached. This is most useful should you want to set break
points in the cpu-dominated parsing so that more input can be loaded from the
source.
4. `step_eoc`: steps to the end of the currently available content
5. `explicit_close`: important to call when your manual processing of the data
is complete - it performs important cleanup, especially in the cases involving
parsing of numeric values, and makes sure that the json input was semantically
complete. Throws a `RuntimeError` if the parser was not at a semantically
appropriate endpoint in the content.

### Even more complex usage

All of the classes implemented as part of this library work in concert, but
they are deliberately written as loosely coupled components. Doubtless, there
are other ingenious ways that they can be coupled together, or which might be
useful under specific conditions or requirements.

### Abstract Classes/Interfaces

It should be noted that this library does not presently provide ABCs for
the creation of objects which can be validated to implement the necessary
public interfaces. This may come in a future version of this library.

## License

This library is licensed under an MIT license. I am extremely grateful to my
employers at Exonar for permitting me to release this library, and give back
to the open source community which has given us so much to work with.

Please confirm by looking at the LICENSE file for details.

## Contributing

This code makes a best-effort attempt to adhere to PEP8 and PEP257, but
eschews module level docstrings because we've not found anything that we
could document at that level that would not simply duplicate other docstrings.

The code is writing using British English localisation. We may opt to add
international English aliases at a later time.
