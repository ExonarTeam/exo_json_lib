
import json
from enum import Enum, auto

class ComposerStack(object):
    """A simple size limited stack."""

    def __init__(self, limit=None):
        """Set up the stack, with an optional limit.
        
        Args:
            limit (int): The number of elements permitted in the stack
        """
        self.limit = limit
        self._stack = []

    def append(self, obj):
        """Append an object to the stack.

        Raises:
            RuntimeError: when an attempt to exceed the size limit is made

        Returns:
            None
        """
        if self.limit is not None:
            if len(self._stack)+1 <= self.limit:
                self._stack.append(obj)
            else:
                raise RuntimeError("Stack limit reached")
        else:
            self._stack.append(obj)
        
    def top(self):
        """Return the element at the top of the stack.

        Do not remove the element from the stack.

        Raises:
            IndexError: when list is empty

        Returns:
            Element at the top of the stack.
        """
        return self._stack[-1]

    def pop(self):
        """Return the element at the top of the stack.

        Remove the element from the top of the stack.

        Returns:
            Element from the top of the stack

        Raises:
            IndexError: when list is empty
        """
        return self._stack.pop()

class StandardComposerStates(Enum):
    """States in which the standard composer may be."""

    EXPECTING_VALUE = auto()
    EXPECTING_OBJECT_KEY_OR_END = auto()
    EXPECTING_OBJECT_VALUE = auto()
    EXPECTING_ARRAY_VALUE_OR_END = auto()
    END = auto()

def nop_cast(obj):
    """Returns the first argument as-is."""
    return obj

class StandardComposer(object):
    """Composes objects based on events from a Json Parser.

    Made up primarily of methods which are event listeners for the Json Parser
    events.
    """

    def __init__(self, stack_limit=None, object_cast=nop_cast):
        """Initialise a composer object.

        Args:
            stack_limit (int): maximum recursion limit of the instance
            object_cast (callable): function with one argument, which is a 
                dictionary. Expected to return an arbitrary object, which will
                be placed in the object hierarchy instead of the dictionary.
        """
        self.object_stack = ComposerStack(limit=stack_limit)
        self.result = None
        self.key_stack = []
        self.state_stack = [StandardComposerStates.EXPECTING_VALUE]
        self.cast = object_cast

    @property
    def state(self):
        """The current state of the StandardComposer."""
        return self.state_stack[-1]

    def complete(self):
        """Returns true if the composer has closed the top level object."""
        return self.state == StandardComposerStates.END

    def object_start(self):
        """Handler for the opening of a json object"""
        if self.state == StandardComposerStates.EXPECTING_VALUE:
            new_value = dict()
            self.object_stack.append(new_value)
            self.result = new_value
            self.state_stack[-1] = StandardComposerStates.END
            self.state_stack.append(StandardComposerStates.EXPECTING_OBJECT_KEY_OR_END)
        elif self.state == StandardComposerStates.EXPECTING_OBJECT_VALUE:
            new_object = dict()
            self.object_stack.append(new_object)
            self.state_stack[-1] = StandardComposerStates.EXPECTING_OBJECT_KEY_OR_END
            self.state_stack.append(StandardComposerStates.EXPECTING_OBJECT_KEY_OR_END)
        elif self.state == StandardComposerStates.EXPECTING_ARRAY_VALUE_OR_END:
            new_object = dict()
            self.object_stack.top().append(new_object)
            self.object_stack.append(new_object)
            self.state_stack.append(StandardComposerStates.EXPECTING_OBJECT_KEY_OR_END)
        else:
            raise RuntimeError("Unexpected Start of Object Event")

    def object_end(self):
        """Handler for the closing of a json object"""
        if self.state == StandardComposerStates.EXPECTING_OBJECT_KEY_OR_END:
            self.state_stack.pop()
            new_object = self.object_stack.pop()
            if len(self.key_stack) > 0:
                key = self.key_stack.pop()
                self.object_stack.top()[key] = self.cast(new_object)
            else:
                self.result = self.cast(self.result)
        else:
            raise RuntimeError("Unexpected end of object event. State is {}, objects are {}".format(self.state_stack, self.object_stack._stack))

    def string(self, string_value):
        """Handler for a string value being completed by the Parser.

        Args:
            string_value (string): the string value that was read
        """
        if self.state == StandardComposerStates.EXPECTING_VALUE:
            self.result = string_value
            self.state_stack[-1] = StandardComposerStates.END
        elif self.state == StandardComposerStates.EXPECTING_OBJECT_KEY_OR_END:
            self.key_stack.append(string_value)
            self.state_stack[-1] = StandardComposerStates.EXPECTING_OBJECT_VALUE
        elif self.state == StandardComposerStates.EXPECTING_OBJECT_VALUE:
            key = self.key_stack.pop()
            self.object_stack.top()[key] = string_value
            self.state_stack[-1] = StandardComposerStates.EXPECTING_OBJECT_KEY_OR_END
        elif self.state == StandardComposerStates.EXPECTING_ARRAY_VALUE_OR_END:
            self.object_stack.top().append(string_value)
        else:
            raise RuntimeError("Unexpected string value event, state is {}".format(self.state_stack))

    def number(self, number_value):
        """Handler for a numerical value having been completed.

        Args:
            number_value (string): the number value that was read"""

        processed_number_value = json.loads(number_value)

        if self.state == StandardComposerStates.EXPECTING_VALUE:
            self.result = processed_number_value
            self.state_stack[-1] = StandardComposerStates.END
        elif self.state == StandardComposerStates.EXPECTING_OBJECT_VALUE:
            key = self.key_stack.pop()
            self.object_stack.top()[key] = processed_number_value
            self.state_stack[-1] = StandardComposerStates.EXPECTING_OBJECT_KEY_OR_END
        elif self.state == StandardComposerStates.EXPECTING_ARRAY_VALUE_OR_END:
            self.object_stack.top().append(processed_number_value)
        else:
            raise RuntimeError("Unexpected number value event (value of {}). State is {}".format(
                processed_number_value, self.state_stack))

    def false(self):
        """Handler for a boolean false value having been read by the parser"""
        if self.state == StandardComposerStates.EXPECTING_VALUE:
            self.result = False
            self.state_stack[-1] = StandardComposerStates.END
        elif self.state == StandardComposerStates.EXPECTING_OBJECT_VALUE:
            key = self.key_stack.pop()
            self.object_stack.top()[key] = False
            self.state_stack[-1] = StandardComposerStates.EXPECTING_OBJECT_KEY_OR_END
        elif self.state == StandardComposerStates.EXPECTING_ARRAY_VALUE_OR_END:
            self.object_stack.top().append(False)
        else:
            raise RuntimeError("Unexpected boolean false value event")

    def true(self):
        """Handler for a boolean true value having been read by the parser"""
        if self.state == StandardComposerStates.EXPECTING_VALUE:
            self.result = True
            self.state_stack[-1] = StandardComposerStates.END
        elif self.state == StandardComposerStates.EXPECTING_OBJECT_VALUE:
            key = self.key_stack.pop()
            self.object_stack.top()[key] = True
            self.state_stack[-1] = StandardComposerStates.EXPECTING_OBJECT_KEY_OR_END
        elif self.state == StandardComposerStates.EXPECTING_ARRAY_VALUE_OR_END:
            self.object_stack.top().append(True)
        else:
            raise RuntimeError("Unexpected boolean true value event")

    def null(self):
        """Handler for an explicit null being read by the parser."""
        if self.state == StandardComposerStates.EXPECTING_VALUE:
            self.result = None
            self.state_stack[-1] = StandardComposerStates.END
        elif self.state == StandardComposerStates.EXPECTING_OBJECT_VALUE:
            key = self.key_stack.pop()
            self.object_stack.top()[key] = None
            self.state_stack[-1] = StandardComposerStates.EXPECTING_OBJECT_KEY_OR_END
        elif self.state == StandardComposerStates.EXPECTING_ARRAY_VALUE_OR_END:
            self.object_stack.top().append(None)
        else:
            raise RuntimeError("Unexpected boolean true value event")

    def array_start(self):
        """Handler for an open of a json array being read by the parser."""
        if self.state == StandardComposerStates.EXPECTING_VALUE:
            self.result = []
            self.object_stack.append(self.result)
            self.state_stack[-1] = StandardComposerStates.END
            self.state_stack.append(StandardComposerStates.EXPECTING_ARRAY_VALUE_OR_END)
        elif self.state == StandardComposerStates.EXPECTING_OBJECT_VALUE:
            new_list = []
            self.object_stack.top()[self.key_stack.pop()] = new_list
            self.object_stack.append(new_list)
            self.state_stack[-1] = StandardComposerStates.EXPECTING_OBJECT_KEY_OR_END
            self.state_stack.append(StandardComposerStates.EXPECTING_ARRAY_VALUE_OR_END)
        elif self.state == StandardComposerStates.EXPECTING_ARRAY_VALUE_OR_END:
            new_list = []
            self.object_stack.top().append(new_list)
            self.object_stack.append(new_list)
            self.state_stack.append(StandardComposerStates.EXPECTING_ARRAY_VALUE_OR_END)
        else:
            raise RuntimeError("Unexpected array start event")

    def array_end(self):
        """Handler for a closing of a json array being read by the parser."""
        if self.state == StandardComposerStates.EXPECTING_ARRAY_VALUE_OR_END:
            self.state_stack.pop()
            self.object_stack.pop()
        else:
            raise RuntimeError("Unexpected end of array event. State is {}".format(self.state_stack))
