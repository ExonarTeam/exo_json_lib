

from enum import Enum, auto

WHITESPACE_CHARS = set((' ', '\n', '\r', '\t'))
HEX_DIGITS = set('abcdef') | set('ABCDEF') | set((str(x) for x in range(0, 10)))
DECIMAL_DIGITS = set((str(x) for x in range(0, 10)))

class StepCharState(Enum):
    """An enum describing the step_char method state.

    These are valid return values from the step_char method
    on the Parser class.

    EOC - designates end of content, without implying that the parser
        has completed in a valid state, just that the supplied content
        has been exhausted.
    CHAR - a character was stepped through, but no events were triggered
    EVENT - an event was triggered as a result of the one character step

    """

    EOC = auto() # end-of-content
    CHAR = auto()
    EVENT = auto()

class StepEventState(Enum):
    """An enum for valid state returned from the Parser method step_event.

    EOC - designates end of content, without implying that the parser
        has completed in a valid state, just that the supplied content
        has been exhausted.
    EVENT - an event was triggered
    """

    EOC = auto()
    EVENT = auto()

class ParserState(Enum):
    """Enum represented the various states of the parser."""

    PRE_CONTENT = auto()
    WHITESPACE = auto()
    VALUE = auto()
    OBJECT = auto()
    COLON = auto()
    COMMA_OR_OBJECT_CLOSE = auto()
    PRE_OBJECT_KEY = auto()
    STRING = auto()
    ESCAPED_CHAR = auto()
    HEX_DIGITS = auto()
    ARRAY = auto()
    COMMA_OR_ARRAY_CLOSE = auto()
    TRUE = auto()
    FALSE = auto()
    NULL = auto()
    FIRST_DIGIT = auto()
    DIGIT_OR_DECIMAL_POINT = auto()
    DECIMAL_POINT_OR_END_NUMBER = auto()
    FIRST_FRACTION_DIGIT = auto()
    FRACTION = auto()
    EXPONENT = auto()
    FIRST_EXPONENT_DIGIT = auto()
    EXPONENT_DIGITS = auto()    
    END = auto()

class ParserEventNames(Enum):
    """An enum keying the different Parser events that can be triggered."""

    OBJECT_START = auto()
    OBJECT_END = auto()
    STRING = auto()
    NUMBER = auto()
    TRUE = auto()
    FALSE = auto()
    NULL = auto()
    ARRAY_START = auto()
    ARRAY_END = auto()

class Parser(object):
    """A Json Parsing object.

    The object is modelled on an event handler or callback style programming
    mechanism. Each character is iterated through, changing the state of the
    parser accordingly. When a character implies an event (for example, the
    start of a new json object), any registered callbacks for that event are
    called with appropriate arguments.
    """

    def __init__(self, cdata_iterator):
        """Initialise the Parser.

        Args:
            cdata_iterator (iter): an iterator which whose __next__ method
                returns strings of 1 character in length.
        """
        self.cdata_iterator = cdata_iterator
        self.state_stack = [ParserState.PRE_CONTENT]
        self.actions = {
            ParserState.PRE_CONTENT: self.pre_content,
            ParserState.WHITESPACE: self.whitespace,
            ParserState.VALUE: self.value,
            ParserState.OBJECT: self.object,
            ParserState.COLON: self.colon,
            ParserState.COMMA_OR_OBJECT_CLOSE: self.comma_or_object_close,
            ParserState.PRE_OBJECT_KEY: self.pre_object_key,
            ParserState.STRING: self.string,
            ParserState.ESCAPED_CHAR: self.escaped_char,
            ParserState.HEX_DIGITS: self.hex_digits,
            ParserState.ARRAY: self.array,
            ParserState.COMMA_OR_ARRAY_CLOSE: self.comma_or_array_close,
            ParserState.TRUE: self.true,
            ParserState.FALSE: self.false,
            ParserState.NULL: self.null,
            ParserState.FIRST_DIGIT: self.first_digit,
            ParserState.DIGIT_OR_DECIMAL_POINT: self.digit_or_decimal_point,
            ParserState.DECIMAL_POINT_OR_END_NUMBER: self.decimal_point_or_end_number,
            ParserState.FIRST_FRACTION_DIGIT: self.first_fraction_digit,
            ParserState.FRACTION: self.fraction,
            ParserState.EXPONENT: self.exponent,
            ParserState.FIRST_EXPONENT_DIGIT: self.first_exponent_digit,
            ParserState.EXPONENT_DIGITS: self.exponent_digits,
            ParserState.END:self.end
        }
        self.handlers = {event_name : set() for event_name in ParserEventNames}
        self.char_buffer = []
        self.hex_buffer = []
        self.character_number = 0
        self.line_number = 0

    @property
    def state(self):
        """Returns the state of the Parser."""
        return self.state_stack[-1]

    def register_handler(self, event_name, handler):
        """Register a handler for an event.

        Note that registering the same handler twice will not result in that
        handler being called twice. The handlers are maintained as a Set.

        Args:
            event_name (ParserEventName): the event for which a handler
                is being registered
            handler (callable): The handler being registered. See the
                StandardComposer class for reference signatures.
        """
        self.handlers[event_name].add(handler)

    def unregister_handler(self, event_name, handler):
        """Unregister a handler for a specified event.

        Args:
            event_name (ParserEventName): the event for which the handler is
                being unregistered
            handler (callable): The handler being unregistered.

        Raises:
            KeyError: if the handler was not registered in the first place.
        """ 
        self.handlers[event_name].remove(handler)

    def call_handlers(self, event_name, *args, **kwargs):
        """Call the handlers with the event name, passing in args and kwargs"""
        for handler in self.handlers[event_name]:
            handler(*args, **kwargs)

    def step_char(self):
        """Iterate one character of the supplied data.

        Performs any appropriately implied actions by the character.

        Returns:
            StepCharState.EVENT: the character triggered an event
            StepCharState.EOC: the content supply has been exhausted
            StepCharState.CHAR: under other conditions
        
        Raises:
            ValueError: an invalid character was encountered for the current
                Parser state.
        """
        return_value = StepCharState.CHAR
        try:
            char = next(self.cdata_iterator)
            self.character_number += 1
            event_triggered = self.actions[self.state](char)
            if event_triggered:
                return_value = StepCharState.EVENT
        except ValueError as e:
            raise ValueError("Error encountered on Line {}, character {}, parser state {}".format(
                self.line_number, self.character_number, self.state_stack)) from e
        except StopIteration:
            return_value = StepCharState.EOC

        return return_value 

    def step_event(self):
        """Iterate through characters until end-of-content or an event.

        Returns:
            StepEventState.EOC: end-of-content was reached
            StepEventState.EVENT: an event was triggered

        Raises:
            ValueError: Indicates a logic fault with this library
        """
        char_status = self.step_char()
        while char_status is StepCharState.CHAR:
            char_status = self.step_char()
        if char_status is StepCharState.EOC:
            return StepEventState.EOC
        elif char_status is StepCharState.EVENT:
            return StepEventState.EVENT
        else:
            raise ValueError('Invalid Step Char State Encountered')
            
    def step_to_eoc(self):
        """Iterate through characters until content is exhausted

        Returns:
            None

        Raises:
            ValueError: An invalid character was found in the json library
                OR a logic error occurred in the code.
        """
        event_status = self.step_event()
        while event_status is not StepEventState.EOC:
            event_status = self.step_event()

    def pre_content(self, char):
        """Action performed per char before seeing significant content."""
        event_triggered = False
        if char not in WHITESPACE_CHARS:
            if char == '{':
                self.state_stack[-1] = ParserState.END
                self.state_stack.append(ParserState.OBJECT)
                self.call_handlers(ParserEventNames.OBJECT_START)
                event_triggered = True
            elif char == '"':
                self.state_stack[-1] = ParserState.END
                self.state_stack.append(ParserState.STRING)
            elif char == '[':
                self.state_stack[-1] = ParserState.END
                self.state_stack.append(ParserState.ARRAY)
                self.call_handlers(ParserEventNames.ARRAY_START)
            elif char == 't':
                self.char_buffer.append(char)
                self.state_stack[-1] = ParserState.END
                self.state_stack.append(ParserState.TRUE)
            elif char == 'f':
                self.char_buffer.append(char)
                self.state_stack[-1] = ParserState.END
                self.state_stack.append(ParserState.FALSE)
            elif char == 'n':
                self.char_buffer.append(char)
                self.state_stack[-1] = ParserState.END
                self.state_stack.append(ParserState.NULL)
            elif char == '-': # all valid beginnings to numbers
                self.char_buffer.append(char)
                self.state_stack[-1] = ParserState.END
                self.state_stack.append(ParserState.FIRST_DIGIT)
            elif char in set((str(x) for x in range(1, 10))):
                self.char_buffer.append(char)
                self.state_stack[-1] = ParserState.END
                self.state_stack.append(ParserState.DIGIT_OR_DECIMAL_POINT)
            elif char == '0':
                self.char_buffer.append(char)
                self.state_stack[-1] = ParserState.END
                self.state_stack.append(ParserState.DECIMAL_POINT_OR_END_NUMBER)
            else:
                raise ValueError("Unexpected {} prior to body content".format(char))
        elif char == '\n':
            self.line_number += 1
            self.char_number = 0
        return event_triggered

    def object(self, char):
        """Actions performed per char while processing a JSON object."""
        event_triggered = False
        if char not in WHITESPACE_CHARS:
            if char == '}':
                self.state_stack.pop()
                self.call_handlers(ParserEventNames.OBJECT_END)
                event_triggered = True
            elif char == '"':
                self.state_stack.append(ParserState.COLON)
                self.state_stack.append(ParserState.WHITESPACE)
                self.state_stack.append(ParserState.STRING)
            else:
                raise ValueError("Unexpected {} in object definition".format(char))
        elif char == '\n':
            self.char_number = 0
            self.line_number += 1
        return event_triggered

    def string(self, char):
        """Object performed per char while processing a json string."""
        event_triggered = False
        unicode_number = ord(char)
        if unicode_number >= 20 and unicode_number <= 0x10FFFF: # taken from www.json.org
            if char == "\\":
                self.state_stack.append(ParserState.ESCAPED_CHAR)
            elif char == '"':
                self.call_handlers(ParserEventNames.STRING, ''.join(self.char_buffer))
                self.char_buffer = []
                event_triggered = True
                self.state_stack.pop()
            else:
                self.char_buffer.append(char)
        elif char == '\n':
            raise ValueError("Unescaped newline")
        elif char == '\t':
            raise ValueError("Unescaped tab")
        else:
            raise ValueError("Invalid unicode character, possibly control, in string")
        return event_triggered

    def escaped_char(self, char):
        """Action performed per char while processing an escaped char."""
        if char in set(('"', '\\', '/')):
            self.char_buffer.append(char)
            self.state_stack.pop()
        elif char == 'b':
            self.char_buffer.append('\b')
            self.state_stack.pop()
        elif char == 'f':
            self.char_buffer.append('\f')
            self.state_stack.pop()
        elif char == 'n':
            self.char_buffer.append('\n')
            self.state_stack.pop()
        elif char == 'r':
            self.char_buffer.append('\r')
            self.state_stack.pop()
        elif char == 't':
            self.char_buffer.append('\t')
            self.state_stack.pop()
        elif char == 'u':
            self.state_stack[-1] = ParserState.HEX_DIGITS
        else:
            raise ValueError('Invalid escape character {}'.format(char))
        return False # never triggers events

    def hex_digits(self, char):
        """Action performed per char while processing hexadecimal digits."""
        if char in HEX_DIGITS:
            self.hex_buffer.append(char)
        else:
            raise ValueError('Invalid hex digit {} not a member of {}'.format(char, HEX_DIGITS))

        if len(self.hex_buffer) == 4:
            hex_string = ''.join(self.hex_buffer)
            i = int(hex_string, 16)
            self.char_buffer.append(chr(i))
            self.hex_buffer = []
            self.state_stack.pop()
        return False

    def whitespace(self, char):
        """Action performed per char, processing nonfunctional whitespace."""
        event_triggerd = False
        if char not in WHITESPACE_CHARS:
            self.state_stack.pop()
            event_triggered = self.actions[self.state](char)
        elif char == '\n':
            self.line_number += 1
            self.char_number = 0
        return event_triggered

    def colon(self, char):
        """Action performed per char, when expecting a key/value separator."""
        if char == ':':
            self.state_stack.pop()
            self.state_stack.append(ParserState.COMMA_OR_OBJECT_CLOSE)
            self.state_stack.append(ParserState.VALUE)
        else:
            raise ValueError("Expected colon, got {}".format(char))
        return False

    def comma_or_object_close(self, char):
        """Action performed per char while between key value pairs."""
        event_triggered = False
        if char not in WHITESPACE_CHARS:
            if char == ',':
                self.state_stack[-1] = ParserState.PRE_OBJECT_KEY
            elif char == '}':
                self.state_stack.pop() # pop current state
                popped_state = self.state_stack.pop() # should be object
                if popped_state != ParserState.OBJECT:
                    raise ValueError("Invalid parser state when closing object")
                self.call_handlers(ParserEventNames.OBJECT_END)
                event_triggered=True
            else:
                raise ValueError("Expecting comma or end of object, found {}. State is {}".format(char, self.state_stack))
        elif char == '\n':
            self.line_number += 1
            self.char_number = 0
        return event_triggered
        
    def pre_object_key(self, char):
        """Action performed while expecting the start of an object key."""
        if char not in WHITESPACE_CHARS:
            if char == '"':
                self.state_stack.pop()
                self.state_stack.append(ParserState.COLON)
                self.state_stack.append(ParserState.WHITESPACE)
                self.state_stack.append(ParserState.STRING)
            else:
                raise ValueError("Unexpected {}, expected \"".format(char))
        elif char == '\n':
            self.line_number += 1
            self.char_number = 0
        return False
            
    def value(self, char):
        """Action performed per character; expecting the start of any value."""
        event_triggered = False
        if char not in WHITESPACE_CHARS:
            if char == '"':
                self.state_stack[-1] = ParserState.STRING 
            elif char == '{':
                self.state_stack[-1] = ParserState.OBJECT
                self.call_handlers(ParserEventNames.OBJECT_START)
                event_triggered = True
            elif char == '[':
                self.state_stack[-1] = ParserState.ARRAY
                self.call_handlers(ParserEventNames.ARRAY_START)
                event_triggered = True
            elif char == 't':
                self.char_buffer.append(char)
                self.state_stack[-1] = ParserState.TRUE
            elif char == 'f':
                self.char_buffer.append(char)
                self.state_stack[-1] = ParserState.FALSE
            elif char == 'n':
                self.char_buffer.append(char)
                self.state_stack[-1] = ParserState.NULL
            elif char == '-': # all valid beginnings to numbers
                self.char_buffer.append(char)
                self.state_stack[-1] = ParserState.FIRST_DIGIT
            elif char in set((str(x) for x in range(1, 10))):
                self.char_buffer.append(char)
                self.state_stack[-1] = ParserState.DIGIT_OR_DECIMAL_POINT
            elif char == '0':
                self.char_buffer.append(char)
                self.state_stack[-1] = ParserState.DECIMAL_POINT_OR_END_NUMBER
            else:
                raise ValueError('Unexpected {}'.format(char))
        elif char == '\n':
            self.char_number = 0
            self.line_number += 1
        return event_triggered

    def array(self, char):
        "Action performed per character when parsing content in a json array."""
        event_triggered = False
        if char not in WHITESPACE_CHARS:
            if char == '"':
                self.state_stack.append(ParserState.COMMA_OR_ARRAY_CLOSE)
                self.state_stack.append(ParserState.STRING)
            elif char == '{':
                self.state_stack.append(ParserState.COMMA_OR_ARRAY_CLOSE)
                self.state_stack.append(ParserState.OBJECT)
                self.call_handlers(ParserEventNames.OBJECT_START)
                event_triggered = True
            elif char == '[':
                self.state_stack.append(ParserState.COMMA_OR_ARRAY_CLOSE)
                self.state_stack.append(ParserState.ARRAY)
                self.call_handlers(ParserEventNames.ARRAY_START)
                event_triggered = True
            elif char == 't':
                self.char_buffer.append(char)
                self.state_stack.append(ParserState.COMMA_OR_ARRAY_CLOSE)
                self.state_stack.append(ParserState.TRUE)
            elif char == 'f':
                self.char_buffer.append(char)
                self.state_stack.append(ParserState.COMMA_OR_ARRAY_CLOSE)
                self.state_stack.append(ParserState.FALSE)
            elif char == 'n':
                self.char_buffer.append(char)
                self.state_stack.append(ParserState.COMMA_OR_ARRAY_CLOSE)
                self.state_stack.append(ParserState.NULL)
            elif char == '-': # all valid beginnings to numbers
                self.state_stack.append(ParserState.COMMA_OR_ARRAY_CLOSE)
                self.char_buffer.append(char)
                self.state_stack.append(ParserState.FIRST_DIGIT)
            elif char in set((str(x) for x in range(1, 10))):
                self.state_stack.append(ParserState.COMMA_OR_ARRAY_CLOSE)
                self.char_buffer.append(char)
                self.state_stack.append(ParserState.DIGIT_OR_DECIMAL_POINT)
            elif char == '0':
                self.state_stack.append(ParserState.COMMA_OR_ARRAY_CLOSE)
                self.char_buffer.append(char)
                self.state_stack.append(ParserState.DECIMAL_POINT_OR_END_NUMBER)
            elif char == ']':
                self.state_stack.pop()
                self.call_handlers(ParserEventNames.ARRAY_END)
                event_triggered=True
            else:
                raise ValueError('Unexpected {}'.format(char))
        elif char == '\n':
            self.char_number = 0
            self.line_number += 1
        return event_triggered 

    def comma_or_array_close(self, char):
        """Action performed per character after an array value has completed."""
        event_triggered = False
        if char not in WHITESPACE_CHARS:
            if char == ',':
                self.state_stack.append(ParserState.VALUE)
            elif char == ']':
                self.state_stack.pop()
                array_state = self.state_stack.pop()
                if array_state != ParserState.ARRAY:
                    raise ValueError("Invalid state when encountering array close")
                self.call_handlers(ParserEventNames.ARRAY_END)
            else:
                raise ValueError("Unexpected {}, expected comma or array closure".format(char))
        elif char == '\n':
            self.line_number += 1
            self.char_number = 0
        return event_triggered

    def true(self, char):
        """Action performed per character while parsing a boolean true."""
        event_triggered = False
        last_char = self.char_buffer[-1]
        if last_char == 't':
            if char != 'r':
                raise ValueError("Expected r, found {}".format(char))
            self.char_buffer.append(char)
        elif last_char == 'r':
            if char != 'u':
                raise ValueError("Expected u, found {}".format(char))
            self.char_buffer.append(char)
        elif last_char  == 'u':
            if char == 'e':
                self.call_handlers(ParserEventNames.TRUE)
                self.state_stack.pop()
                event_triggered = True
                self.char_buffer = []
            else:
                raise ValueError("Expected e, found {}".format(char))
        else:
            raise RuntimeError("Invalid state encountered, char buffer is {}".format(self.char_buffer))
        return event_triggered

    def false(self, char):
        """Action performed per character while parsing a boolean false."""
        event_triggered = False
        last_char = self.char_buffer[-1]
        if last_char == 'f':
            if char != 'a':
                raise ValueError("Expected a, found {}".format(char))
            self.char_buffer.append(char)
        elif last_char == 'a':
            if char != 'l':
                raise ValueError("Expected l, found {}".format(char))
            self.char_buffer.append(char)
        elif last_char == 'l':
            if char != 's':
                raise ValueError("Expected s, found {}".format(char))
            self.char_buffer.append(char)
        elif last_char == 's':
            if char == 'e':
                self.call_handlers(ParserEventNames.FALSE)
                self.state_stack.pop()
                event_triggered = True
                self.char_buffer = []
            else:
                raise ValueError("Expected e, found {}".format(char))
        else:
            raise RuntimeError("Invalid state encountered, char buffer is {}".format(self.char_buffer))
        return event_triggered
            
    def null(self, char):
        """Action performed per character while parsing an explicit null."""
        event_triggered = False
        last_char = self.char_buffer[-1]
        if last_char == 'n':
            if char != 'u':
                raise ValueError("Expected u, found {}".format(char))
            self.char_buffer.append(char)
        elif last_char == 'u':
            if char != 'l':
                raise ValueError("Expected l, found {}".format(char))
            self.char_buffer.append(char)
        elif last_char == 'l':
            if char == 'l':
                self.call_handlers(ParserEventNames.NULL) 
                event_triggered=True
                self.char_buffer = []
                self.state_stack.pop()
            else:
                raise ValueError("Expected l, found {}".format(char))
        else:
            raise RuntimeError("Invalid state encountered, char buffer is {}".format(self.char_buffer))
        return event_triggered
        
    def first_digit(self, char):
        """Action performed per character on the first digit of a numeric."""
        if char == '0':
            self.char_buffer.append(char)
            self.state_stack[-1] = ParserState.DECIMAL_POINT_OR_END_NUMBER
        elif char in set((str(x) for x in range(1, 10))):
            self.char_buffer.append(char)
            self.state_stack[-1] = ParserState.DIGIT_OR_DECIMAL_POINT
        else:
            raise ValueError("Unexpected {}, expected digit")
        return False #never raises events

    def digit_or_decimal_point(self, char):
        """Action performed per value when expecting a digit or decimal pt."""
        event_triggered = False
        if char in DECIMAL_DIGITS:
            self.char_buffer.append(char)
        elif char == '.':
            self.char_buffer.append(char)
            self.state_stack[-1] = ParserState.FIRST_FRACTION_DIGIT
        elif char in WHITESPACE_CHARS:
            self.call_handlers(ParserEventNames.NUMBER, ''.join(self.char_buffer))
            self.state_stack.pop()
            self.char_buffer = []
            event_triggered = True
            if char == '\n':
                self.char_number = 0
                self.line_number += 1
        elif char in set(('e', 'E')):
            self.char_buffer.append(char)
            self.state_stack[-1] = ParserState.EXPONENT
        elif char == ',':
            self.call_handlers(ParserEventNames.NUMBER, ''.join(self.char_buffer))
            self.char_buffer = []
            self.state_stack.pop()
            popped_state = self.state_stack[-1]
            if popped_state == ParserState.COMMA_OR_ARRAY_CLOSE:
                self.state_stack.append(ParserState.VALUE)
            elif popped_state == ParserState.COMMA_OR_OBJECT_CLOSE:
                self.state_stack[-1] = ParserState.PRE_OBJECT_KEY
            else:
                raise ValueError("Unexpected comma delimiter")
            event_triggered = True
        elif char == '}':
            self.call_handlers(ParserEventNames.NUMBER, ''.join(self.char_buffer))
            self.char_buffer = []
            self.state_stack.pop()
            event_triggered = True
            popped_state = self.state_stack.pop()
            if popped_state != ParserState.COMMA_OR_OBJECT_CLOSE:
                raise ValueError("Unexpected object closure - state is {}".format(self.state_stack))
            popped_state = self.state_stack.pop()
            if popped_state != ParserState.OBJECT:
                raise ValueError("Unexpected object closure - state is {}".format(self.state_stack))
            self.call_handlers(ParserEventNames.OBJECT_END)
        elif char == ']':
            self.call_handlers(ParserEventNames.NUMBER, ''.join(self.char_buffer))
            self.char_buffer = []
            self.state_stack.pop()
            event_triggered = True
            popped_state = self.state_stack.pop()
            if popped_state != ParserState.COMMA_OR_ARRAY_CLOSE:
                raise ValueError("Unexpected array closure. Popped state was {}.".format(popped_state))
            popped_state == self.state_stack.pop()
            self.call_handlers(ParserEventNames.ARRAY_END)
        else:
            raise ValueError("Unexpected {}, expected decimal digit or point".format(char))
        return event_triggered

    def decimal_point_or_end_number(self, char):
        """Action performed per char; expecting a decimal pt. or last digit."""
        event_triggered = False
        if char == '.':
            self.char_buffer.append(char)
            self.state_stack[-1] = ParserState.FIRST_FRACTION_DIGIT
        elif char in WHITESPACE_CHARS:
            event_triggered = True
            self.call_handlers(ParserEventNames.NUMBER, ''.join(self.char_buffer))
            self.char_buffer = []
            self.state_stack.pop()
        elif char in ('e', 'E'):
            self.char_buffer.append(char)
            self.state_stack[-1] = ParserState.EXPONENT
        elif char == ']':
            self.call_handlers(ParserEventNames.NUMBER, ''.join(self.char_buffer))
            self.char_buffer = []
            self.state_stack.pop()
            event_triggered = True
            popped_state = self.state_stack.pop()
            if popped_state != ParserState.COMMA_OR_ARRAY_CLOSE:
                raise ValueError("Unexpected array closure. Popped state was {}".format(popped_state))
            popped_state_two = self.state_stack.pop()
            if popped_state_two != ParserState.ARRAY:
                raise ValueError("Unexpected array closure. Popped state was {}. State stack was {}".format(popped_state_two, self.state_stack))
            self.call_handlers(ParserEventNames.ARRAY_END)
        elif char == '}':
            self.call_handlers(ParserEventNames.NUMBER, ''.join(self.char_buffer))
            self.char_buffer = []
            self.state_stack.pop()
            event_triggered = True
            popped_state = self.state_stack.pop()
            if popped_state != ParserState.COMMA_OR_OBJECT_CLOSE:
                raise ValueError("Unexpected object closure - state is {}".format(self.state_stack))
            popped_state = self.state_stack.pop()
            if popped_state != ParserState.OBJECT:
                raise ValueError("Unexpected object closure - state is {}".format(self.state_stack))
            self.call_handlers(ParserEventNames.OBJECT_END)
        else:
            raise ValueError("Unexpected {}, expected end of number or decimal point".format(char))
        return event_triggered
            

    def first_fraction_digit(self, char):
        """Action performed expecting digit after a decimal point."""
        event_triggered = False
        if char in DECIMAL_DIGITS:
            self.char_buffer.append(char)
            self.state_stack[-1] = ParserState.FRACTION
        else:
            raise ValueError("Unexpected {}, expected decimal digit".format(char))
        return event_triggered

    def fraction(self, char):
        """Action performed per char while processing digits after a d.p."""
        event_triggered = False
        if char in DECIMAL_DIGITS:
            self.char_buffer.append(char)
        elif char in WHITESPACE_CHARS:
            self.call_handlers(ParserEventNames.NUMBER, ''.join(self.char_buffer))
            self.state_stack.pop()
            event_triggered = True
            if char == '\n':
                self.line_number += 1
                self.char_number = 0
            self.char_buffer = []
        elif char in set(('e', 'E')):
            self.char_buffer.append(char)
            self.state_stack[-1] = ParserState.EXPONENT
        elif char == ',':
            self.call_handlers(ParserEventNames.NUMBER, ''.join(self.char_buffer))
            self.char_buffer = []
            self.state_stack.pop()
            popped_state = self.state_stack[-1]
            if popped_state == ParserState.COMMA_OR_ARRAY_CLOSE:
                self.state_stack.append(ParserState.VALUE)
            elif popped_state == ParserState.COMMA_OR_OBJECT_CLOSE:
                self.state_stack[-1] = ParserState.PRE_OBJECT_KEY
            else:
                raise ValueError("Unexpected comma delimiter")
            event_triggered = True
        elif char == '}':
            self.call_handlers(ParserEventNames.NUMBER, ''.join(self.char_buffer))
            self.char_buffer = []
            self.state_stack.pop()
            event_triggered = True
            popped_state = self.state_stack.pop()
            if popped_state != ParserState.COMMA_OR_OBJECT_CLOSE:
                raise ValueError("Unexpected object closure - state is {}".format(self.state_stack))
            popped_state = self.state_stack.pop()
            if popped_state != ParserState.OBJECT:
                raise ValueError("Unexpected object closure - state is {}".format(self.state_stack))
            self.call_handlers(ParserEventNames.OBJECT_END)
        elif char == ']':
            self.call_handlers(ParserEventNames.NUMBER, ''.join(self.char_buffer))
            self.char_buffer = []
            self.state_stack.pop()
            event_triggered = True
            popped_state = self.state_stack.pop()
            if popped_state != ParserState.COMMA_OR_ARRAY_CLOSE:
                raise ValueError("Unexpected array closure. Popped state was {}".format(popped_state))
            popped_state_two = self.state_stack.pop()
            if popped_state_two != ParserState.ARRAY:
                raise ValueError("Unexpected array closure. Popped state was {}. State stack was {}".format(popped_state_two, self.state_stack))
            self.call_handlers(ParserEventNames.ARRAY_END)
        else:
            raise ValueError("Unexpected {}, expected decimal digit, whitespace or exponent 'e'".format(char))
        return event_triggered

    def exponent(self, char):
        """Action performed per character when expecting an exponent"""
        if char in DECIMAL_DIGITS:
            self.char_buffer.append(char)
            self.state_stack[-1] = ParserState.EXPONENT_DIGITS
        elif char in ('-', '+'):
            self.char_buffer.append(char)
            self.state_stack[-1] = ParserState.FIRST_EXPONENT_DIGIT
        else:
            raise ValueError('Unexpected {}, expected decimal digit or sign'.format(char))

    def first_exponent_digit(self, char):
        """Action performed per char when expecting the first exponent digit."""
        event_triggered = False
        if char in DECIMAL_DIGITS:
            self.char_buffer.append(char)
            self.state_stack[-1] = ParserState.EXPONENT_DIGITS
        else:
            raise ValueError("Unexpected {}, expected decimal digit or whitespace".format(char))
        return event_triggered

    def exponent_digits(self, char):
        """Action performed per char when processing exponent digits."""
        event_triggered = False
        if char in DECIMAL_DIGITS:
            self.char_buffer.append(char)
        elif char in WHITESPACE_CHARS:
            self.call_handlers(ParserEventNames.NUMBER, ''.join(self.char_buffer))
            self.state_stack.pop()
            self.char_buffer = []
            event_triggered = True
            if char == '\n':
                self.line_number += 1
                self.char_number = 0
        elif char == ',':
            self.call_handlers(ParserEventNames.NUMBER, ''.join(self.char_buffer))
            self.char_buffer = []
            self.state_stack.pop()
            popped_state = self.state_stack.pop()
            if popped_state not in (ParserState.COMMA_OR_ARRAY_CLOSE, ParserState.COMMA_OR_OBJECT_CLOSE):
                raise ValueError("Unexpected comma delimiter")
            event_triggered = True
        elif char == '}':
            self.call_handlers(ParserEventNames.NUMBER, ''.join(self.char_buffer))
            self.char_buffer = []
            self.state_stack.pop()
            event_triggered = True
            popped_state = self.state_stack.pop()
            if popped_state != ParserState.COMMA_OR_OBJECT_CLOSE:
                raise ValueError("Unexpected object closure - state is {}".format(self.state_stack))
            popped_state = self.state_stack.pop()
            if popped_state != ParserState.OBJECT:
                raise ValueError("Unexpected object closure - state is {}".format(self.state_stack))
            self.call_handlers(ParserEventNames.OBJECT_END)
        elif char == ']':
            self.call_handlers(ParserEventNames.NUMBER, ''.join(self.char_buffer))
            self.char_buffer = []
            self.state_stack.pop()
            event_triggered = True
            popped_state = self.state_stack.pop()
            if popped_state != ParserState.COMMA_OR_ARRAY_CLOSE:
                raise ValueError("Unexpected array closure")
            popped_state = self.state_stack.pop()
            if popped_state != ParserState.ARRAY:
                raise ValueError("Unexpected array closure. Popped state was {}".format(popped_state))
            self.call_handlers(ParserEventNames.ARRAY_END)
        else:
            raise ValueError("Unexpected {}, expected decimal digit or whitespace".format(char))
        return event_triggered

    def end(self, char):
        """Action performed at end; expecting no further significant chars."""
        if char not in WHITESPACE_CHARS:
            raise ValueError("Unexpected {} after main content".format(char))
        elif char == '\n':
            self.char_number = 0
            self.line_number += 1

    def explicit_close(self):
        """Raises a useful error if content has not ended validly."""
        state = self.state_stack.pop()
        while state != ParserState.END:
            if state == ParserState.PRE_CONTENT:
                raise RuntimeError("Explicit close when no content found")
            elif state == ParserState.WHITESPACE:
                raise RuntimeError("Explicit close when expecting explicit whitespace")
            elif state == ParserState.VALUE:
                raise RuntimeError("Explicit close called when expecting value data")
            elif state == ParserState.OBJECT:
                raise RuntimeError("Explicit close when expecting object data")
            elif state == ParserState.COLON:
                raise RuntimeError("Explicit close when expecting colon")
            elif state == ParserState.COMMA_OR_OBJECT_CLOSE:
                raise RuntimeError("Explicit close when expecting comma or object close")
            elif state == ParserState.COMMA_OR_ARRAY_CLOSE:
                raise RuntimeError("Explicit close when expecting comma or array close")
            elif state == ParserState.PRE_OBJECT_KEY:
                raise RuntimeError("Explicit close when expecting object key")
            elif state == ParserState.STRING:
                raise RuntimeError("Explicit close when expecting string closure")
            elif state == ParserState.ESCAPED_CHAR:
                raise RuntimeError("Explicit close when expecting string closure")
            elif state == ParserState.HEX_DIGITS:
                raise RuntimeError("Explicit close when expecting string closure")
            elif state == ParserState.ARRAY:
                raise RuntimeError("Explicit close when array not finished")
            elif state == ParserState.TRUE:
                raise RuntimeError("Explicit close when true incomplete") 
            elif state == ParserState.FALSE:
                raise RuntimeError("Explicit close when false incomplete") 
            elif state == ParserState.FALSE:
                raise RuntimeError("Explicit close when null incomplete") 
            elif state == ParserState.FIRST_DIGIT:
                raise RuntimeError("Explicit close when expecting digit")
            elif state == ParserState.DIGIT_OR_DECIMAL_POINT:
                self.call_handlers(ParserEventNames.NUMBER, ''.join(self.char_buffer))
                self.char_buffer = []
                state = self.state_stack.pop() 
            elif state == ParserState.DECIMAL_POINT_OR_END_NUMBER:
                self.call_handlers(ParserEventNames.NUMBER, ''.join(self.char_buffer))
                self.char_buffer = []
                state = self.state_stack.pop() 
            elif state == ParserState.FIRST_FRACTION_DIGIT:
                raise RuntimeError("Explicit close when expecting first digit of a fraction")
            elif state == ParserState.FRACTION:
                self.call_handlers(ParserEventNames.NUMBER, ''.join(self.char_buffer))
                self.char_buffer = []
                state = self.state_stack.pop() 
            elif state == ParserState.EXPONENT:
                raise RuntimeError("Explicit close when expecting first digit of exponent")
            elif state == ParserState.EXPONENT_DIGITS:
                self.call_handlers(ParserEventNames.NUMBER, ''.join(self.char_buffer))
                self.char_buffer = []
                state = self.state_stack.pop() 
            elif state == ParserState.END:
                return
            else:
                raise ValueError("Unknown state {} at explicit closure".format(state))
