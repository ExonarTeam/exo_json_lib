
from enum import Enum, auto
import json


def json_escape_string(string):
    """Escape a string for json."""
    # The feeling is that this would eventually be replaced by our own
    # implementation of the dumps function, but for simple string values this
    # gives us a dependably correct escaping of those strings relatively
    # cheaply
    return json.dumps(string)

def should_serialise_as_object(obj):
    """Identify that a python object should serialise to a json object."""
    return hasattr(obj, 'items')

def should_serialise_as_array(obj):
    """Identify that a python object should serialise as a json array."""
    return isinstance(obj, list) or isinstance(obj, tuple)

def should_serialise_as_number(obj):
    """Identify that a python object should serialise as a json number (int)."""
    return isinstance(obj, int)

def should_serialise_as_float(obj):
    """Identify that a python object should serialise as a json float."""
    return isinstance(obj, float)

def should_serialise_as_string(obj):
    """Identify that a python object should serialise as a json string."""
    return isinstance(obj, str)

def should_serialise_as_bool(obj):
    """Identify that a python object should serialise as a json bool."""
    return isinstance(obj, bool)

def should_serialise_as_null(obj):
    """Identify that a python object should serialise as a json null."""
    return obj is None

def should_serialise_as_value(obj):
    """Identify that a python object implements a json value interface.

    Such python objects are serialisation aware, and so will proffer a value
    of a suitable type to be serialised.
    """
    return hasattr(obj, 'json_value')

def should_serialise_as_raw_value(obj):
    """Identify that a python object implements a raw json value interface.

    Such python objects are serialisation aware, and will proffer a raw string
    value that requires no further serialisation.
    """
    return hasattr(obj, 'raw_json_value')


class StringRecipient(object):
    """An class which can act as a recipient of string data from the composer.

    This particular recipient simply allows the conversion of sent data into
    a string. The key signature of a recipient is the receive method, if you
    should need to implement your own recipient to serve another purpose (e.g.
    async io).
    """ 

    def __init__(self):
        """Simple initialiser."""
        self.buffer = []

    def receive(self, cdata):
        """Recieve additional character data"""
        self.buffer.append(cdata)

    def __str__(self):
        """Output the buffer to a string. Does not deplete the buffer."""
        return ''.join(self.buffer)


class WorkAction(Enum):
    """An enum representing the labels of different work item actions.

    This internal enum represents the actions to be performed on an object
    which requires serialisation or other further processing.

    EMIT is the final piece of processing, and simply submits the appropriate
    string data to the recipient.

    """

    MAKE_OBJECT = auto()
    MAKE_ARRAY = auto()
    MAKE_INTEGER = auto()
    MAKE_FLOAT = auto()
    MAKE_STRING = auto()
    MAKE_BOOL = auto()
    MAKE_NULL = auto()
    # for cases where we want to emit a string
    # directly - for example, closing an object
    # or array
    EMIT = auto()

class WorkItem(object):
    """An item in the single-linked list of work.

    To prevent recursion, serialisation is represented as a singly-linked
    list of actions to be performed (WorkItems). Thus, processing a complex
    object can yield one or more additional work items which can be inserted
    into the list in O(1) time, and no recursion need necessarily be performed.
    """

    def __init__(self, action, target):
        """Initialiser for a work item.

        Arguments:
            action (WorkAction) - the action to be performed
            target (Object) - the object on which the action is performed
        """
        self.action = action
        self.target = target
        self._next = None

    def attach(self, other):
        """Insert an item between this item and the next item.

        If no next item exist, then simply add the item as the next item. This
        method should be safe to use with chains of work items, but it does not
        seem that scenario presently exists in the codebase.
        """
        if self._next is not None:
            other.attach(self.next())
        self._next = other

    def next(self):
        """Return the next item in the chain."""
        return self._next

class Serialiser(object):
    """A class for serialising python objects into a json string."""

    def __init__(self, cdata_recipient, custom_callbacks=None, callback_chooser=type):
        """Initialise the Serialiser.

        Args:
            cdata_recipient (Object): receives the character data output from
                actions performed while serialising. Character data is always
                sent in the correct order. See StringRecipient class for the
                relevant interface.
            custom_callbacks (Dictionary): a dictionary of callbacks; the keys
                are some kind of type signature (defined by callback_chooser),
                and the values are callables which will take the target object
                as an argument. Note that custom callbacks take precedence
                over the default callbacks.
            callback_chooser (callable): a callable that accepts an object, and
                returns a key which can be looked up in custom callbacks.
        """
        self.recipient = cdata_recipient
        if custom_callbacks is None:
            self.custom_callbacks = {}
        else:
            self.custom_callbacks = custom_callbacks
        self.callback_chooser = callback_chooser

    def create_work_item(self, obj):
        """Add a workitem into the workqueue to serialise an object."""
        callback_label = self.callback_chooser(obj)
        if callback_label in self.custom_callbacks:
            return WorkItem(WorkAction.EMIT, self.custom_callbacks[callback_label](obj))
        elif should_serialise_as_object(obj):
            return WorkItem(WorkAction.MAKE_OBJECT, obj)
        elif should_serialise_as_array(obj):
            return WorkItem(WorkAction.MAKE_ARRAY, obj)
        elif should_serialise_as_bool(obj):
            return WorkItem(WorkAction.MAKE_BOOL, obj)
        elif should_serialise_as_number(obj):
            return WorkItem(WorkAction.MAKE_INTEGER, obj)
        elif should_serialise_as_float(obj):
            return WorkItem(WorkAction.MAKE_FLOAT, obj)
        elif should_serialise_as_string(obj):
            return WorkItem(WorkAction.MAKE_STRING, obj)
        elif should_serialise_as_null(obj):
            return WorkItem(WorkAction.MAKE_NULL, obj)
        elif should_serialise_as_value(obj):
            return self.create_work_item(obj.json_value())
        elif should_serialise_as_raw_value(obj):
            return WorkItem(WorkAction.EMIT, obj.raw_json_value())
        else:
            raise ValueError("{} is not a serialisable object type".format(type(obj)))

    def serialise(self, obj):
        """Serialise the provided object into valid json.

        Args:
           obj (Object): the python object to be serialised, progressively
            sending character data to the recipient.
        """ 

        work_item = self.create_work_item(obj)

        while work_item is not None:
            if work_item.action == WorkAction.EMIT:
                self.recipient.receive(work_item.target)
            elif work_item.action == WorkAction.MAKE_OBJECT:
                object_start = WorkItem(WorkAction.EMIT, '{')
                work_item.attach(object_start)
                attach_point = object_start
                
                key_value_pairs = list(work_item.target.items())
                if key_value_pairs:
                    last_pair = key_value_pairs[-1]
                    
                    for key, value in key_value_pairs[:-1]:
                        if not isinstance(key, str):
                            raise KeyError("Object keys must be string types")
                        key_emit = WorkItem(WorkAction.EMIT, '"{}":'.format(key))
                        attach_point.attach(key_emit)
                        next_item = self.create_work_item(value)
                        key_emit.attach(next_item)
                        separator_emit = WorkItem(WorkAction.EMIT, ',')
                        next_item.attach(separator_emit)
                        attach_point = separator_emit
                    
                    key = last_pair[0]
                    value = last_pair[1]
    
                    if not isinstance(key, str):
                        raise KeyError("Object keys must be string types")
                    key_emit = WorkItem(WorkAction.EMIT, '"{}":'.format(key))
                    attach_point.attach(key_emit)
                    next_item = self.create_work_item(value)
                    key_emit.attach(next_item)
                    attach_point = next_item
                
                object_close = WorkItem(WorkAction.EMIT, '}')
                attach_point.attach(object_close)

            elif work_item.action == WorkAction.MAKE_ARRAY:
                object_start = WorkItem(WorkAction.EMIT, '[')
                work_item.attach(object_start)
                attach_point = object_start

                iter_values = work_item.target[:-1]
                last_value = work_item.target[-1]

                for value in iter_values:
                    next_item = self.create_work_item(value)
                    attach_point.attach(next_item)
                    comma_emit = WorkItem(WorkAction.EMIT, ',')
                    next_item.attach(comma_emit)
                    attach_point = comma_emit

                last_item = self.create_work_item(last_value)
                attach_point.attach(last_item)
                object_close = WorkItem(WorkAction.EMIT, ']')
                last_item.attach(object_close)

            elif work_item.action == WorkAction.MAKE_INTEGER:
                emit_int = WorkItem(WorkAction.EMIT, str(work_item.target))
                work_item.attach(emit_int)

            elif work_item.action == WorkAction.MAKE_FLOAT:
                emit_exp = WorkItem(WorkAction.EMIT, str(work_item.target))
                work_item.attach(emit_exp)

            elif work_item.action == WorkAction.MAKE_STRING:
                emit_string = WorkItem(WorkAction.EMIT, json_escape_string(work_item.target))
                work_item.attach(emit_string)

            elif work_item.action == WorkAction.MAKE_BOOL:
                emit_bool = WorkItem(WorkAction.EMIT, str(bool(work_item.target)).lower())
                work_item.attach(emit_bool)

            elif work_item.action == WorkAction.MAKE_NULL:
                emit_null = WorkItem(WorkAction.EMIT, 'null')
                work_item.attach(emit_null)

            else:
                raise ValueError("Invalid member of WorkAction Enum")
                
            work_item = work_item.next()    

