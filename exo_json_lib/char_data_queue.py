
from queue import Queue

class CharDataQueue(object):
    """A class which allows multiple strings to be broken up into characters.

    This is particularly useful when processing content asynchronously or
    in a chunked fashion. An instance of this class can be passed to the
    parser constructor, and the data can be submitted to this class. Multiple
    calls to the parser step_to_eoc method can be performed asynchronously
    of the addition of data to this iterator.
    """

    def __init__(self):
        """Simple initialiser."""
        self.queue = Queue()
        self.current_cdata = None
        self.idx = 0

    def put(self, cdata_iterator):
        """Add string data to the queue."""
        self.queue.put_nowait(cdata_iterator)

    def __iter__(self):
        """Return self as the iterator."""
        return self

    def __next__(self):
        """Return the next character. Iterate through queued strings."""
        if self.current_cdata is None or self.idx >= len(self.current_cdata):
            if self.queue.empty():
                raise StopIteration()
            else:
                self.idx = 0
                self.current_cdata = self.queue.get_nowait()

        c = self.current_cdata[self.idx]
        self.idx += 1
        return c
