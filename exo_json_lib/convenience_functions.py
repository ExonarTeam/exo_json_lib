
from .standard_composer import StandardComposer, nop_cast
from .parser import Parser, ParserEventNames
from .serialiser import Serialiser, StringRecipient
from .char_data_queue import CharDataQueue

def create_parser_composer_pair(
        parser_factory=Parser,
        composer_factory=StandardComposer,
        object_cast=nop_cast,
        cdata_iterator=None,
        nesting_limit=None):

    if cdata_iterator is None:
        cdata_iterator = CharDataQueue()

    parser = parser_factory(cdata_iterator)
    composer = composer_factory(nesting_limit, object_cast=object_cast)
    parser.register_handler(ParserEventNames.OBJECT_START, composer.object_start)
    parser.register_handler(ParserEventNames.OBJECT_END, composer.object_end)
    parser.register_handler(ParserEventNames.STRING, composer.string)
    parser.register_handler(ParserEventNames.NUMBER, composer.number)
    parser.register_handler(ParserEventNames.TRUE, composer.true)
    parser.register_handler(ParserEventNames.FALSE, composer.false)
    parser.register_handler(ParserEventNames.NULL, composer.null)
    parser.register_handler(ParserEventNames.ARRAY_START, composer.array_start)
    parser.register_handler(ParserEventNames.ARRAY_END, composer.array_end)
    return cdata_iterator, parser, composer

def serialise(obj, custom_callbacks=None, callback_chooser=type):
    recipient = StringRecipient()
    serialiser = Serialiser(recipient, custom_callbacks, callback_chooser)
    serialiser.serialise(obj)
    return str(recipient)

def deserialise(
            string,
            composer_factory=StandardComposer,
            object_cast=nop_cast,
            nesting_limit=None
        ):

    _, parser, composer = create_parser_composer_pair(
        Parser,
        composer_factory,
        object_cast,
        iter(string),
        nesting_limit
    )

    parser.step_to_eoc()
    parser.explicit_close()
    if composer.complete():
        return composer.result
    else:
        raise RuntimeError("Composer was left in non-concluded state: {}".format(composer.state))

# in case someone needs to switch from the old json lib
loads = deserialise
dumps = serialise
